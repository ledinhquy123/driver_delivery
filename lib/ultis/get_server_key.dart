import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:googleapis/servicecontrol/v1.dart' as servicecontrol;
import 'package:googleapis_auth/auth_io.dart' as auth;
import 'package:shared_preferences/shared_preferences.dart';

void getServerKey() async {
  final serviceAccountJson = {
    "type": "service_account",
    "project_id": "testnotifications-2efc1",
    "private_key_id": "b04735973ce2b4df299819bf8987b89a82ffa181",
    "private_key":
        "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCVcWg5RBEbP6BN\nRQiQxCqMfyfId4+BIkXQZtahP3XQS2yi1PxQqTbKaQFb54sagWkaFUaHIiwXnONu\n9kaWD/8BR4QVPsf+oy+sBctszVAJMTUbMrm2EvvMejr43m0SsDvYOmz3uQESBBkc\nNLPPmXeQ/0TOUgHmkeOSRlLMP8s8CNySzl5V7qqN4PvnZWKU6LK6kRdl9aU/SGz+\n9hn3UF8ay2UDZofweI9FTTdWjorFMc/l0vrKuqlyZt63eYSnMmrG2ffltu8qaX1W\n5v0SZ3eizReUszvv2Zg+Pe+1r0qZ0KR+KgKETkoQ/KU2zDhJ400sygNowRVMzioM\ngQS6lV1XAgMBAAECggEABday9D3RD0f6mto+He3UHb4gD0iAjlKI+L3z18Pzdepk\nea4pl3ss/VaB65XNzQDOWusG4OO4ZknGZwDvPBCu7V1ebzgPN0cn5ygozPzuihVm\nlkPylocCWHhUEoIaL9c711RdCoarYIlRc9UpS0ybK8qVZRw2H8yGYCIoPPunFJbS\nwfeWsbw4FLkEJTEYNucbU4odavYux+nmJqOJVXchYdShwaaOFgBugXYFKWWR3DeP\nDW0UDmLphNcLgfg1/I73jl+EE62OzxtC+uaQoMz7ssqlb3yFDlmKXiPSqPxmOwFb\nVqCiKlyQXBaYesEBYbMkmt5Rl72Kpxm1Y3PfGy7iPQKBgQDHW+idYfgJ2AWjDMtt\n2grFGdQjNRvEEnS0ay/lR+dl2KrEvdJKzAlMem3Bz5Jv2vscWRHFtRN3AIwBzFhX\noYxuj66aTrQFqiT3B1xOG2Ly/yjp9dyU20KkBufj2T3fXaWPS3DgUuwoG8Kslv5h\nPq7MNPSe924bRMALGcY48zFL/QKBgQC/5u2vn70OsdkPjCSJzRTLa4EerMRPPnSi\nE+4zYqDtkIK/iZWqNyETT/6KbndnxW3QS7RPyGvePGizUzzGXXxhwvwhO3c9nkE3\ndbrSOLGVm8ImTwIwndprRl03BZQ9jbGSeamB3+0iJIWMtdnska0mVaxBF/pnmIpy\nnZ7LrGWs4wKBgQDGim3bb1oK06ypeXCPcnsPWG3Asn94gbjghW8ptLZq46EOVs07\n2H/aPoNYZVS9OSSkUsA0WeWMumq0z8GbZDPrWATXgFIg4hX2Pspar0OYJS2l80k+\npi50FLyUWiIYdbcQQl2jynGOcDcskyO7zSVgEc0dE+YAGjP32i9jQC90MQKBgQCe\nCzv5hd8YORSBH3QAIWxc0971Omg6sz8vNKbanwHWZUupH1CpbNnKo/kIXU3tosV7\n5L1Nb8yWrL214nmNDPUhNoqbe/QKRUmEuWrxM0vd4PeWwT8Wdwqpiru+UtPxlb6G\n2bfmhFsx4U4zwpkFojwt9CT3Px41L4XmOzXKm+Lo7QKBgQC4FKl4n4P0bUGtfHde\nYkQhpjH7bKksqk737jGYwTA0GeDzZYJ+Bfzj2F3TWIkPhdXNfzaI2x5b/hyTGKzh\nyUdguT0URFOHZJdV+8JpxtEdeeUWNmDrnniusegdRd6Z6dzWnDW3XSaIvmzEsZnd\ngKxg7/bzhXGpfM02fcL7rjS8sg==\n-----END PRIVATE KEY-----\n",
    "client_email":
        "firebase-adminsdk-hzdu2@testnotifications-2efc1.iam.gserviceaccount.com",
    "client_id": "115119348464755808916",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url":
        "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-hzdu2%40testnotifications-2efc1.iam.gserviceaccount.com",
    "universe_domain": "googleapis.com"
  };

  List<String> scopes = [
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/firebase.database",
    "https://www.googleapis.com/auth/firebase.messaging"
  ];

  http.Client client = await auth.clientViaServiceAccount(
    auth.ServiceAccountCredentials.fromJson(serviceAccountJson),
    scopes,
  );

  // Obtain the access token
  auth.AccessCredentials credentials =
      await auth.obtainAccessCredentialsViaServiceAccount(
          auth.ServiceAccountCredentials.fromJson(serviceAccountJson),
          scopes,
          client);

  // Close the HTTP client
  client.close();

  // Return the access token
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('serverKey', credentials.accessToken.data);
  log('SERVERKEY: ${credentials.accessToken.data}');
}
